<%@page import="java.sql.ResultSet"%>
<%@ page import="com.demeter.snippets.enterprise.dao.*"%>

<HTML>
<HEAD>
</HEAD>

<BODY>
	<h1>The Number of Registrations Summary</h1>

	<div> Registrations:
		<%=Database.getInstance().getNumberOfSubscribers() %> 
	</div>
	<br>

	<%
		ResultSet resultSet = Database.getInstance().getSummary();
	%>
	<div>Top 5 registered countries:</div>
	<TABLE BORDER="1">
		<TR>
			<TH>Country</TH>
			<TH>Number of Registrations</TH>
		</TR>
		<%
			for (int i = 0; i < 5; i++) {
				if (resultSet.next()) {
		%>
		<TR align="center">
			<TD><%=resultSet.getString(1)%></td>
			<TD><%=resultSet.getString(2)%></TD>
		</TR>
		<%
			}
			}
		%>
	</TABLE>
</BODY>
</HTML>
package com.demeter.snippets.enterprise.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.sql.Statement;

import com.demeter.snippets.enterprise.model.Subscriber;

public class Database {

	private Connection connection;
	private PreparedStatement prepStatInsert;
	private PreparedStatement prepStatDelete;
	private String user;
	private String password;

	private static Database db = null;

	private Database() {
		this.user = "root";
		this.password = "mysql";
	}

	public static Database getInstance() throws SQLException {
		if (db == null) {
			db = new Database();
			db.connect();
		}
		return db;
	}

	private void connect() throws SQLException {
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		this.connection = DriverManager.getConnection(
				"jdbc:mysql://localhost:3306/newsletter?useSSL=false&useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC",
				this.user, this.password);
		prepStatDelete = connection.prepareStatement("DELETE FROM subscribers WHERE email = ?");
		prepStatInsert = connection.prepareStatement("INSERT INTO subscribers VALUES(?,?,?,?)");
	}

	public void saveNewSubscriber(Subscriber subscriber) throws SQLException {
		prepStatInsert.setString(1, subscriber.getEmail());
		prepStatInsert.setString(2, subscriber.getName());
		prepStatInsert.setString(3, subscriber.getLastName());
		prepStatInsert.setString(4, subscriber.getCountry());
		try {
			prepStatInsert.executeUpdate();
		} catch (SQLIntegrityConstraintViolationException e) {
			// duplicate Primary Key (2 times the same email address)
		}
	}

	public int deleteSubscriber(String email) throws SQLException {
		prepStatDelete.setString(1, email);
		return prepStatDelete.executeUpdate();
	}

	public ResultSet getSummary() throws SQLException {
		Statement statement = connection.createStatement();
		return statement.executeQuery(
				"SELECT country, COUNT(*) AS summary FROM subscribers GROUP BY country ORDER BY summary DESC");
	}

	public int getNumberOfSubscribers() throws SQLException {
		Statement statement = connection.createStatement();
		ResultSet resultSet = statement.executeQuery("SELECT COUNT(*) AS number FROM subscribers");
		resultSet.next();
		return resultSet.getInt(1);
	}

}

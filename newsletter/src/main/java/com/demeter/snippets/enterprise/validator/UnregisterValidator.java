package com.demeter.snippets.enterprise.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.demeter.snippets.enterprise.model.Subscriber;

public class UnregisterValidator implements Validator {

	public boolean supports(Class<?> paramClass) {
		return Subscriber.class.equals(paramClass);
	}

	public void validate(Object obj, Errors errors) {
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "email", "valid.email");
		Subscriber subscriber = (Subscriber) obj;
		if (!subscriber.getEmail().contains("@")) {
			errors.rejectValue("email", "valid.emailIncorrect");
		}
	}
}
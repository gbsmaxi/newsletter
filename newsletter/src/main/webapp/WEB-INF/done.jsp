<html>
<body>
	<h2>Thanks for registration</h2>
	<br>
	<table>
		<tr>
			<td>First Name:</td>
			<td>${subscriber.name}</td>
		</tr>
		<tr>
			<td>Last Name:</td>
			<td>${subscriber.lastName}</td>
		</tr>
		<tr>
			<td>E-Mail:</td>
			<td>${subscriber.email}</td>
		</tr>
		<tr>
			<td>Country:</td>
			<td>${subscriber.country}</td>
		</tr>
	</table>
	<br>
	<div>You just signed up successfully for newsletters!</div>
</body>
</html>
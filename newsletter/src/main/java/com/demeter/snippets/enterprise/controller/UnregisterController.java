package com.demeter.snippets.enterprise.controller;

import java.sql.SQLException;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.demeter.snippets.enterprise.dao.Database;
import com.demeter.snippets.enterprise.model.Subscriber;

@Controller
@RequestMapping("/unregister.htm")
public class UnregisterController {

	//@Autowired
	@Qualifier("unregisterValidator")
	private Validator validator;

	@InitBinder
	private void initBinder(WebDataBinder binder) {
		binder.setValidator(validator);
	}

	@RequestMapping(method = RequestMethod.GET)
	public String initForm(Model model) {
		Subscriber subscriber = new Subscriber();
		model.addAttribute("subscriber", subscriber);
		return "unregister";
	}

	@RequestMapping(method = RequestMethod.POST)
	public String submitForm(Model model, @Validated Subscriber subscriber, BindingResult result) {
		String returnVal = "unregisteredDone";
		if (result.hasErrors()) {
			returnVal = "unregister";
		} else {
			try {
				Database.getInstance().deleteSubscriber(subscriber.getEmail());
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return returnVal;
	}

}
package com.demeter.snippets.enterprise.controller;

import java.sql.SQLException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.demeter.snippets.enterprise.dao.Database;
import com.demeter.snippets.enterprise.model.Subscriber;

@Controller
@RequestMapping("/subscriber.htm")
public class SubscriberController {

	@Autowired
	@Qualifier("subscriberValidator")
	private Validator validator;

	@InitBinder
	private void initBinder(WebDataBinder binder) {
		binder.setValidator(validator);
	}

	@RequestMapping(method = RequestMethod.GET)
	public String initForm(Model model) {
		Subscriber subscriber = new Subscriber();
		model.addAttribute("subscriber", subscriber);
		return "subscriber";
	}

	@RequestMapping(method = RequestMethod.POST)
	public String submitForm(Model model, @Validated Subscriber subscriber, BindingResult result) {
		String returnVal = "done";
		if (result.hasErrors()) {
			returnVal = "subscriber";
		} else {
			model.addAttribute("subscriber", subscriber);
			try {
				Database.getInstance().saveNewSubscriber(subscriber);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return returnVal;
	}
}
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

	<html>
	<head>
	</head>
	 
	<body>
		<div align="right"><a href="\newsletter\unregister.htm">Unregister</a></div>
	    <h2>Sign up for Newsletter</h2>
	 	
	    <form:form method="POST" commandName="subscriber">
	        <table> 
	            <tr>
	                <td>First Name: </td>
	                <td><form:input path="name" /></td>
	                <td><form:errors path="name" cssStyle="color: #ff0000;"/></td>
	            </tr>
	            <tr>
	                <td>Last Name: </td>
	                <td><form:input path="lastName" /></td>
	                <td><form:errors path="lastName" cssStyle="color: #ff0000;"/></td>
	            </tr>
	         	<tr>
	                <td>Email address: </td>
	                <td><form:input path="email" /></td>
	                <td><form:errors path="email" cssStyle="color: #ff0000;"/></td>
	            </tr>
	            
	            <!-- <tr>
	                <td>Country: </td>
	                <td><form:input path="country" /></td>
	                <td><form:errors path="country" cssStyle="color: #ff0000;"/></td>
	            </tr> -->
	            
	            <tr>
	            	<td>Country: </td>
	            	<td>
	            	<form:select path="country">
      					<form:option value="-" label="--- Please Select ---"/>
      					<form:option value="Afghanistan" label="Afghanistan"/>
						<form:option value="Egypt" label="Egypt"/>
						<form:option value="�land Islands" label="�land Islands"/>
						<form:option value="Albania" label="Albania"/>
						<form:option value="Algeria" label="Algeria"/>
						<form:option value="American Samoa" label="American Samoa"/>
						<form:option value="Virgin Islands, U.s." label="Virgin Islands, U.s."/>
						<form:option value="Andorra" label="Andorra"/>
						<form:option value="Angola" label="Angola"/>
						<form:option value="Anguilla" label="Anguilla"/>
						<form:option value="Antarctica" label="Antarctica"/>
						<form:option value="Antigua And Barbuda" label="Antigua And Barbuda"/>
						<form:option value="Equatorial Guinea" label="Equatorial Guinea"/>
						<form:option value="Argentina" label="Argentina"/>
						<form:option value="Armenia" label="Armenia"/>
						<form:option value="Aruba" label="Aruba"/>
						<form:option value="Ascension" label="Ascension"/>
						<form:option value="Azerbaijan" label="Azerbaijan"/>
						<form:option value="Ethiopia" label="Ethiopia"/>
						<form:option value="Australia" label="Australia"/>
						<form:option value="Bahamas" label="Bahamas"/>
						<form:option value="Bahrain" label="Bahrain"/>
						<form:option value="Bangladesh" label="Bangladesh"/>
						<form:option value="Barbados" label="Barbados"/>
						<form:option value="Belgium" label="Belgium"/>
						<form:option value="Belize" label="Belize"/>
						<form:option value="Benin" label="Benin"/>
						<form:option value="Bermuda" label="Bermuda"/>
						<form:option value="Bhutan" label="Bhutan"/>
						<form:option value="Bolivia" label="Bolivia"/>
						<form:option value="Bosnia And Herzegovina" label="Bosnia And Herzegovina"/>
						<form:option value="Botswana" label="Botswana"/>
						<form:option value="Bouvet Island" label="Bouvet Island"/>
						<form:option value="Brazil" label="Brazil"/>
						<form:option value="Brunei Darussalam" label="Brunei Darussalam"/>
						<form:option value="Bulgaria" label="Bulgaria"/>
						<form:option value="Burkina Faso" label="Burkina Faso"/>
						<form:option value="Burundi" label="Burundi"/>
						<form:option value="Chile" label="Chile"/>
						<form:option value="China" label="China"/>
						<form:option value="Cook Islands" label="Cook Islands"/>
						<form:option value="Costa Rica" label="Costa Rica"/>
						<form:option value="C�te D'ivoire" label="C�te D'ivoire"/>
						<form:option value="Denmark" label="Denmark"/>
						<form:option value="Germany" label="Germany"/>
						<form:option value="Diego Garcia" label="Diego Garcia"/>
						<form:option value="Dominica" label="Dominica"/>
						<form:option value="Dominican Republic" label="Dominican Republic"/>
						<form:option value="Djibouti" label="Djibouti"/>
						<form:option value="Ecuador" label="Ecuador"/>
						<form:option value="El Salvador" label="El Salvador"/>
						<form:option value="Eritrea" label="Eritrea"/>
						<form:option value="Estonia" label="Estonia"/>
						<form:option value="Europ�ische Union" label="Europ�ische Union"/>
						<form:option value="Falkland Islands (malvinas)" label="Falkland Islands (malvinas)"/>
						<form:option value="Faroe Islands" label="Faroe Islands"/>
						<form:option value="Fiji" label="Fiji"/>
						<form:option value="Finland" label="Finland"/>
						<form:option value="France" label="France"/>
						<form:option value="French Guiana" label="French Guiana"/>
						<form:option value="French Polynesia" label="French Polynesia"/>
						<form:option value="Gabon" label="Gabon"/>
						<form:option value="Gambia" label="Gambia"/>
						<form:option value="Georgia" label="Georgia"/>
						<form:option value="Ghana" label="Ghana"/>
						<form:option value="Gibraltar" label="Gibraltar"/>
						<form:option value="Grenada" label="Grenada"/>
						<form:option value="Greece" label="Greece"/>
						<form:option value="Greenland" label="Greenland"/>
						<form:option value="Create Britain" label="Create Britain"/>
						<form:option value="Guadeloupe" label="Guadeloupe"/>
						<form:option value="Guam" label="Guam"/>
						<form:option value="Guatemala" label="Guatemala"/>
						<form:option value="Guernsey" label="Guernsey"/>
						<form:option value="Guinea" label="Guinea"/>
						<form:option value="Guinea-bissau" label="Guinea-bissau"/>
						<form:option value="Guyana" label="Guyana"/>
						<form:option value="Haiti" label="Haiti"/>
						<form:option value="Heard Island And Mcdonald Islands" label="Heard Island And Mcdonald Islands"/>
						<form:option value="Honduras" label="Honduras"/>
						<form:option value="Hong Kong" label="Hong Kong"/>
						<form:option value="India" label="India"/>
						<form:option value="Indonesia" label="Indonesia"/>
						<form:option value="Iraq" label="Iraq"/>
						<form:option value="Iran, Islamic Republic Of" label="Iran, Islamic Republic Of"/>
						<form:option value="Ireland" label="Ireland"/>
						<form:option value="Iceland" label="Iceland"/>
						<form:option value="Israel" label="Israel"/>
						<form:option value="Italy" label="Italy"/>
						<form:option value="Jamaica" label="Jamaica"/>
						<form:option value="Japan" label="Japan"/>
						<form:option value="Yemen" label="Yemen"/>
						<form:option value="Jersey" label="Jersey"/>
						<form:option value="Jordan" label="Jordan"/>
						<form:option value="Cayman Islands" label="Cayman Islands"/>
						<form:option value="Cambodia" label="Cambodia"/>
						<form:option value="Cameroon" label="Cameroon"/>
						<form:option value="Canada" label="Canada"/>
						<form:option value="Kanarische Inseln" label="Kanarische Inseln"/>
						<form:option value="Cape Verde" label="Cape Verde"/>
						<form:option value="Kazakhstan" label="Kazakhstan"/>
						<form:option value="Qatar" label="Qatar"/>
						<form:option value="Kenya" label="Kenya"/>
						<form:option value="Kyrgyzstan" label="Kyrgyzstan"/>
						<form:option value="Kiribati" label="Kiribati"/>
						<form:option value="Cocos (keeling) Islands" label="Cocos (keeling) Islands"/>
						<form:option value="Colombia" label="Colombia"/>
						<form:option value="Comoros" label="Comoros"/>
						<form:option value="Congo" label="Congo"/>
						<form:option value="Croatia" label="Croatia"/>
						<form:option value="Cuba" label="Cuba"/>
						<form:option value="Kuwait" label="Kuwait"/>
						<form:option value=" lao People's Democratic Republic" label=" lao People's Democratic Republic"/>
						<form:option value=" lesotho" label=" lesotho"/>
						<form:option value=" latvia" label=" latvia"/>
						<form:option value=" lebanon" label=" lebanon"/>
						<form:option value=" liberia" label=" liberia"/>
						<form:option value=" libyan Arab Jamahiriya" label=" libyan Arab Jamahiriya"/>
						<form:option value=" liechtenstein" label=" liechtenstein"/>
						<form:option value=" lithuania" label=" lithuania"/>
						<form:option value=" luxembourg" label=" luxembourg"/>
						<form:option value="Macao" label="Macao"/>
						<form:option value="Madagascar" label="Madagascar"/>
						<form:option value="Malawi" label="Malawi"/>
						<form:option value="Malaysia" label="Malaysia"/>
						<form:option value="Maldives" label="Maldives"/>
						<form:option value="Mali" label="Mali"/>
						<form:option value="Malta" label="Malta"/>
						<form:option value="Morocco" label="Morocco"/>
						<form:option value="Marshall Islands" label="Marshall Islands"/>
						<form:option value="Martinique" label="Martinique"/>
						<form:option value="Mauritania" label="Mauritania"/>
						<form:option value="Mauritius" label="Mauritius"/>
						<form:option value="Mayotte" label="Mayotte"/>
						<form:option value="Macedonia, The Former Yugoslav Republic Of" label="Macedonia, The Former Yugoslav Republic Of"/>
						<form:option value="Mexico" label="Mexico"/>
						<form:option value="Micronesia" label="Micronesia"/>
						<form:option value="Moldova" label="Moldova"/>
						<form:option value="Monaco" label="Monaco"/>
						<form:option value="Mongolia" label="Mongolia"/>
						<form:option value="Montserrat" label="Montserrat"/>
						<form:option value="Mozambique" label="Mozambique"/>
						<form:option value="Myanmar" label="Myanmar"/>
						<form:option value="Namibia" label="Namibia"/>
						<form:option value="Nauru" label="Nauru"/>
						<form:option value="Nepal" label="Nepal"/>
						<form:option value="New Caledonia" label="New Caledonia"/>
						<form:option value="New Zealand" label="New Zealand"/>
						<form:option value="Neutrale Zone" label="Neutrale Zone"/>
						<form:option value="Nicaragua" label="Nicaragua"/>
						<form:option value="Netherlands" label="Netherlands"/>
						<form:option value="Netherlands Antilles" label="Netherlands Antilles"/>
						<form:option value="Niger" label="Niger"/>
						<form:option value="Nigeria" label="Nigeria"/>
						<form:option value="Niue" label="Niue"/>
						<form:option value="North Korea" label="North Korea"/>
						<form:option value="Northern Mariana Islands" label="Northern Mariana Islands"/>
						<form:option value="Norfolk Island" label="Norfolk Island"/>
						<form:option value="Norway" label="Norway"/>
						<form:option value="Oman" label="Oman"/>
						<form:option value="Austria" label="Austria"/>
						<form:option value="Pakistan" label="Pakistan"/>
						<form:option value="Palestinian Territory" label="Palestinian Territory"/>
						<form:option value="Palau" label="Palau"/>
						<form:option value="Panama" label="Panama"/>
						<form:option value="Papua New Guinea" label="Papua New Guinea"/>
						<form:option value="Paraguay" label="Paraguay"/>
						<form:option value="Peru" label="Peru"/>
						<form:option value="Philippines" label="Philippines"/>
						<form:option value="Pitcairn" label="Pitcairn"/>
						<form:option value="Poland" label="Poland"/>
						<form:option value="Portugal" label="Portugal"/>
						<form:option value="Puerto Rico" label="Puerto Rico"/>
						<form:option value="R�union" label="R�union"/>
						<form:option value="Rwanda" label="Rwanda"/>
						<form:option value="Romania" label="Romania"/>
						<form:option value="Russian Federation" label="Russian Federation"/>
						<form:option value="Solomon Islands" label="Solomon Islands"/>
						<form:option value="Zambia" label="Zambia"/>
						<form:option value="Samoa" label="Samoa"/>
						<form:option value="San Marino" label="San Marino"/>
						<form:option value="Sao Tome And Principe" label="Sao Tome And Principe"/>
						<form:option value="Saudi Arabia" label="Saudi Arabia"/>
						<form:option value="Sweden" label="Sweden"/>
						<form:option value="Switzerland" label="Switzerland"/>
						<form:option value="Senegal" label="Senegal"/>
						<form:option value="Serbien und Montenegro" label="Serbien und Montenegro"/>
						<form:option value="Seychelles" label="Seychelles"/>
						<form:option value="Sierra Leone" label="Sierra Leone"/>
						<form:option value="Zimbabwe" label="Zimbabwe"/>
						<form:option value="Singapore" label="Singapore"/>
						<form:option value="Slovakia" label="Slovakia"/>
						<form:option value="Slovenia" label="Slovenia"/>
						<form:option value="Somalia" label="Somalia"/>
						<form:option value="Spain" label="Spain"/>
						<form:option value="Sri Lanka" label="Sri Lanka"/>
						<form:option value="Saint Helena" label="Saint Helena"/>
						<form:option value="Saint Kitts And Nevis" label="Saint Kitts And Nevis"/>
						<form:option value="Saint Lucia" label="Saint Lucia"/>
						<form:option value="Saint Pierre And Miquelon" label="Saint Pierre And Miquelon"/>
						<form:option value="Saint Vincent/Grenadines" label="Saint Vincent/Grenadines"/>
						<form:option value="South Africa" label="South Africa"/>
						<form:option value="Sudan" label="Sudan"/>
						<form:option value="South Korea" label="South Korea"/>
						<form:option value="Suriname" label="Suriname"/>
						<form:option value="Svalbard And Jan Mayen" label="Svalbard And Jan Mayen"/>
						<form:option value="Swaziland" label="Swaziland"/>
						<form:option value="Syrian Arab Republic" label="Syrian Arab Republic"/>
						<form:option value="Tajikistan" label="Tajikistan"/>
						<form:option value="Taiwan, Province Of China" label="Taiwan, Province Of China"/>
						<form:option value="Tanzania" label="Tanzania"/>
						<form:option value="Thailand" label="Thailand"/>
						<form:option value="Timor-leste" label="Timor-leste"/>
						<form:option value="Togo" label="Togo"/>
						<form:option value="Tokelau" label="Tokelau"/>
						<form:option value="Tonga" label="Tonga"/>
						<form:option value="Trinidad And Tobago" label="Trinidad And Tobago"/>
						<form:option value="Tristan da Cunha" label="Tristan da Cunha"/>
						<form:option value="Chad" label="Chad"/>
						<form:option value="Czech Republic" label="Czech Republic"/>
						<form:option value="Tunisia" label="Tunisia"/>
						<form:option value="Turkey" label="Turkey"/>
						<form:option value="Turkmenistan" label="Turkmenistan"/>
						<form:option value="Turks And Caicos Islands" label="Turks And Caicos Islands"/>
						<form:option value="Tuvalu" label="Tuvalu"/>
						<form:option value="Uganda" label="Uganda"/>
						<form:option value="Ukraine" label="Ukraine"/>
						<form:option value="Hungary" label="Hungary"/>
						<form:option value="Uruguay" label="Uruguay"/>
						<form:option value="Uzbekistan" label="Uzbekistan"/>
						<form:option value="Vanuatu" label="Vanuatu"/>
						<form:option value="Holy See (vatican City State)" label="Holy See (vatican City State)"/>
						<form:option value="Venezuela" label="Venezuela"/>
						<form:option value="United Arab Emirates" label="United Arab Emirates"/>
						<form:option value="United States" label="United States"/>
						<form:option value="Viet Nam" label="Viet Nam"/>
						<form:option value="Wallis And Futuna" label="Wallis And Futuna"/>
						<form:option value="Christmas Island" label="Christmas Island"/>
						<form:option value="Belarus" label="Belarus"/>
						<form:option value="Western Sahara" label="Western Sahara"/>
						<form:option value="Central African Republic" label="Central African Republic"/>
						<form:option value="Cyprus" label="Cyprus"/>
      					<form:options items="${countryList}" itemValue="country" itemLabel="country"/>
 					</form:select>
 					</td>
 					<td><form:errors path="country" cssStyle="color: #ff0000;"/></td>
 				</tr>
	            <tr>
	                <td><input type="submit" name="submit" value="Submit"></td>
	            </tr>
	            <tr>
	        </table>
	    </form:form>
	</body>
	</html> 
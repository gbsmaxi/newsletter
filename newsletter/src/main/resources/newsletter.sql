CREATE DATABASE IF NOT EXISTS `newsletter`;

USE newsletter;

CREATE TABLE IF NOT EXISTS subscribers(
email VARCHAR(30) PRIMARY KEY,
name VARCHAR(30),
lastName VARCHAR(30),
country VARCHAR(30));
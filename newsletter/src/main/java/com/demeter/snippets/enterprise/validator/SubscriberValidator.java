package com.demeter.snippets.enterprise.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.demeter.snippets.enterprise.model.Subscriber;

public class SubscriberValidator implements Validator {

	public boolean supports(Class<?> paramClass) {
		return Subscriber.class.equals(paramClass);
	}

	public void validate(Object obj, Errors errors) {
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "valid.name");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "lastName", "valid.lastName");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "email", "valid.email");

		Subscriber subscriber = (Subscriber) obj;
		if (subscriber.getName().length() < 2) {
			errors.rejectValue("name", "valid.nametoShort");
		}
		if (subscriber.getLastName().length() < 2) {
			errors.rejectValue("lastName", "valid.lastNametoShort");
		}
		if (!subscriber.getEmail().contains("@")) {
			errors.rejectValue("email", "valid.emailIncorrect");
		}
		if (subscriber.getCountry().equals("-")){
			errors.rejectValue("country", "valid.chooseCountry");
		}
	}
}